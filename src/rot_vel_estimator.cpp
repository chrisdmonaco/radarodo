/********************************COPYRIGHT CHRIS D. MONACO, 2019**********************************/
#include <opencv2/imgproc/imgproc.hpp>
#include <g2o/core/block_solver.h>
#include <g2o/solvers/csparse/linear_solver_csparse.h>
#include <g2o/core/optimization_algorithm_levenberg.h>

#include <radarodo/rot_vel_estimator.h>
#include <radarodo/heading_change_vertex.h>
#include <radarodo/heading_change_edge_prior.h>
#include <radarodo/competitive_edge.h>

typedef g2o::BlockSolver< g2o::BlockSolverTraits<-1, -1> > SlamBlockSolver;
typedef g2o::LinearSolverCSparse<SlamBlockSolver::PoseMatrixType> SlamLinearSolver;

static constexpr uint kSensorPoseVertexId = 0;

RotVelEstimator::RotVelEstimator(
  const double radians_per_pixel,
  const double extracted_points_weight_fraction_threshold,
  const uint max_num_extracted_points,
  const uint max_num_iterations,
  const bool verbose)
  : radians_per_pixel_(radians_per_pixel),
    extracted_points_weight_fraction_threshold_(extracted_points_weight_fraction_threshold),
    max_num_extracted_points_(max_num_extracted_points),
    max_num_iterations_(max_num_iterations),
    previous_points_stored_(false),
    // Initialize the 1x3 Sobel derivative kernel coefficients
    sobel_kernel_dI_dtheta_((cv::Mat_<double>(1, 3) <<
     + 1.0 / (2.0 * radians_per_pixel), 0, - 1.0 / (2.0 * radians_per_pixel))) {
  optimizer_pose_graph_.setVerbose(verbose);
  Eigen::initParallel();

  // Initialize the optimizer pose graph

  auto linear_solver_unique_ptr = g2o::make_unique<SlamLinearSolver>();
  linear_solver_unique_ptr->setBlockOrdering(false);

  auto slam_block_solver_unique_ptr =
    g2o::make_unique<SlamBlockSolver>(std::move(linear_solver_unique_ptr));
  auto solver_levenberg_unique_ptr =
    g2o::make_unique<g2o::OptimizationAlgorithmLevenberg>(std::move(slam_block_solver_unique_ptr));

  optimizer_pose_graph_.setAlgorithm(std::move(solver_levenberg_unique_ptr).get());
  solver_levenberg_unique_ptr.release();
}

void RotVelEstimator::StoreMeasurement(const cv::Mat& polar_image) {
  const double polar_image_max = GetImageMax(polar_image);
  // Create mask that marks pixels with intensities above a set fraction of the image's
  // max. pixel intensity
  const cv::Mat extracted_points_mask =
    polar_image > (extracted_points_weight_fraction_threshold_ * polar_image_max);

  // Find the indices of all points that have been flagged in the mask
  std::vector<cv::Point2i> extracted_point_indices;
  cv::findNonZero(extracted_points_mask, extracted_point_indices);

  if (!extracted_point_indices.size()) return;

  // Calculate the desired number of extracted points. The number may be saturated to prevent
  // the sampled point number from exceeding the upper limit.
  const uint num_extracted_points =
    extracted_point_indices.size() > max_num_extracted_points_ ?
    max_num_extracted_points_ : extracted_point_indices.size();

  // Randomly sample the extracted pixel points to create a uniformly distributed set that is
  // representative, but its number does not exceed the desired number of extracted points
  RandomlySamplePoints(extracted_point_indices, polar_image, num_extracted_points, polar_image_max);

  previous_points_stored_ = true;
}

void RotVelEstimator::RandomlySamplePoints(
    const std::vector<cv::Point>& extracted_point_indices,
    const cv::Mat& polar_image,
    const uint num_extracted_points,
    const double polar_image_max) {
  extracted_point_data_ = Eigen::Array<double, Eigen::Dynamic, 3>(num_extracted_points, 3);

  // Randomly generate indices of the extracted points to select them for the sample.
  // This is done by generating random values betwen 0-1 and then multiplying (and rounding)
  // those numbers to the higher possible index number.
  const Eigen::Array<int, Eigen::Dynamic, 1> point_selections =
    (Eigen::Array<double, Eigen::Dynamic, 1>::Random(num_extracted_points).abs() *
    extracted_point_indices.size()).cast<int>();

  // For each of the randomly selected points, add that point to the corresponding row
  // in extracted point data. The columns of extracted point data are: image x-coord. (u),
  // y-coord. (v), and the point pixels weight (fraction of its image's max intensity).
  uint selected_point_idx;
  for (uint selection_idx = 0; selection_idx < point_selections.rows(); ++selection_idx) {
    selected_point_idx = point_selections(selection_idx);
    extracted_point_data_.row(selection_idx) <<
      extracted_point_indices[selected_point_idx].x,
      extracted_point_indices[selected_point_idx].y,
      static_cast<double>(polar_image.at<ushort>(extracted_point_indices[selected_point_idx])) /
        polar_image_max;
  }
}

double RotVelEstimator::Estimate(
    const cv::Mat& blurred_polar_image,
    const double delta_t,
    Eigen::Matrix<double, 3, 3>& sensor_motion_covariance_matrix) {
  // Use the Sobel derivative kernel on the blurred polar image to calculat the partial derivative
  // of the image intensity with respect to theta
  cv::Mat dI_dtheta;
  cv::filter2D(blurred_polar_image,
               dI_dtheta,
               CV_64F,
               sobel_kernel_dI_dtheta_);

  // Generates the necessary optimization pose graph by constructing all edge constraints and
  // attaching them to the heading pose node
  GeneratePoseGraph(blurred_polar_image,
                    dI_dtheta);

  // Optimzes via gradient descent to calculate the heading change
  optimizer_pose_graph_.initializeOptimization();
  optimizer_pose_graph_.optimize(max_num_iterations_);

  // Calculates the expected standard deviation of the estimated yaw rate by converting the
  // heading estimate's information in the Hessian matrix to a standard deviation and then
  // applying the conversion factors required when transforming the heading standard deviation
  // to a yaw rate standard deviation [rad/s]
  const double yaw_rate_sigma =
    (1.0 / std::sqrt(optimizer_pose_graph_.vertex(kSensorPoseVertexId)->hessian(0, 0))) / delta_t;

  // Populate sensor motion covariance matrix according to the yaw rate standard deviation.
  // Covariance matrix terms associated with yaw_rate (third row/column) were calculated
  // in the Translational Velocity Estimator but not yet scaled with regards to the
  // yaw rate standard deviation, since it was not yet known. Therefore, scale these terms
  // according to the estimated yaw rate standard deviation.
  sensor_motion_covariance_matrix(2, 2) = yaw_rate_sigma * yaw_rate_sigma;
  sensor_motion_covariance_matrix(0, 2) *= yaw_rate_sigma;
  sensor_motion_covariance_matrix(1, 2) *= yaw_rate_sigma;
  sensor_motion_covariance_matrix(2, 0) = sensor_motion_covariance_matrix(0, 2);
  sensor_motion_covariance_matrix(2, 1) = sensor_motion_covariance_matrix(1, 2);

  // Returns the yaw-rate estimate [rad/s] by dividing the estimated heading change by delta_t
  return static_cast<Radarodo::HeadingChangeVertex*>(
    optimizer_pose_graph_.vertex(kSensorPoseVertexId))->estimate() / delta_t;
}

void RotVelEstimator::GeneratePoseGraph(
    const cv::Mat& blurred_polar_image,
    const cv::Mat& dI_dtheta) {
  // Generates skeleton framework of pose graph by clearing graph, constructing pose node,
  // and attaching a prior edge
  GenerateSkeletonFramework();

  const double polar_image_max = GetImageMax(blurred_polar_image);

  // For each extracted pixel point, construct a edge constraint and attach it to the
  // heading change pose node. Columns in extracted_point_data are u-coord., v-coord.,
  // and pixel point weight
  for (uint row_idx = 0; row_idx < extracted_point_data_.rows(); ++row_idx) {
    auto radarodo_edge_unique_ptr =
      g2o::make_unique<Radarodo::CompetitiveEdge>(
          blurred_polar_image,
          dI_dtheta,
          cv::Point2d(extracted_point_data_(row_idx, 0), extracted_point_data_(row_idx, 1)),
          extracted_point_data_(row_idx, 2),
          radians_per_pixel_,
          polar_image_max,
          optimizer_pose_graph_.vertex(kSensorPoseVertexId));

    optimizer_pose_graph_.addEdge(std::move(radarodo_edge_unique_ptr).get());

    radarodo_edge_unique_ptr.release();
  }
}

void RotVelEstimator::GenerateSkeletonFramework() {
  // Clear optimizer pose graph from previous iteration
  optimizer_pose_graph_.clear();

  // Create heading change sensor pose vertex
  auto sensor_pose_vertex_unique_ptr = g2o::make_unique<Radarodo::HeadingChangeVertex>();
  sensor_pose_vertex_unique_ptr->setToOriginImpl();
  sensor_pose_vertex_unique_ptr->setId(kSensorPoseVertexId);
  optimizer_pose_graph_.addVertex(std::move(sensor_pose_vertex_unique_ptr).get());
  sensor_pose_vertex_unique_ptr.release();

  // Create the prior edge for the sensor pose vertex but give it a negligible weight (information)
  auto prior_edge_unique_ptr = g2o::make_unique<Radarodo::HeadingChangeEdgePrior>();
  prior_edge_unique_ptr->setVertex(0, optimizer_pose_graph_.vertex(kSensorPoseVertexId));
  prior_edge_unique_ptr->setMeasurement(0);
  prior_edge_unique_ptr->setInformation(Eigen::Matrix<double, 1, 1>::Constant(1e-20));
  optimizer_pose_graph_.addEdge(std::move(prior_edge_unique_ptr).get());
  prior_edge_unique_ptr.release();
}
