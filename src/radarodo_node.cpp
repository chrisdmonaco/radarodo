/********************************COPYRIGHT CHRIS D. MONACO, 2019**********************************/
#include <ros/ros.h>

#include <radarodo/egomotion_estimator.h>

int main(int argc, char* argv[]) {
    ros::init(argc, argv, "radarodo");

    // Construct RADARODO ego-motion estimator object to estimate RADAR sensor ego-motion
    EgomotionEstimator egomotion_estimator;

    ros::spin();

    return 0;
}
