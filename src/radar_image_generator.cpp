/********************************COPYRIGHT CHRIS D. MONACO, 2019**********************************/
#include <opencv2/imgproc/imgproc.hpp>

#include <radarodo/radar_image_generator.h>

RadarImageGenerator::RadarImageGenerator(
    const uint polar_image_width,
    const uint polar_image_height,
    const double meters_per_pixel,
    const double radians_per_pixel,
    const double polar_image_u0,
    const double polar_image_v0,
    const double target_range_sigma_meters,
    const double target_azimuth_sigma_radians)
  : polar_image_width_(polar_image_width),
    polar_image_height_(polar_image_height),
    meters_per_pixel_(meters_per_pixel),
    radians_per_pixel_(radians_per_pixel),
    polar_image_u0_(polar_image_u0),
    polar_image_v0_(polar_image_v0),
    target_range_sigma_pixels_(target_range_sigma_meters / meters_per_pixel),
    target_azimuth_sigma_pixels_(target_azimuth_sigma_radians / radians_per_pixel),
    // Kernel size is +/- 3 sigma to mitigate discontinuities and inconsequential summations
    polar_kernel_size_(6 * std::round(target_azimuth_sigma_pixels_),
                       6 * std::round(target_range_sigma_pixels_)) {
  // Make kernel size odd if it is not already
  if (polar_kernel_size_.width % 2 == 0) ++polar_kernel_size_.width;
  if (polar_kernel_size_.height % 2 == 0) ++polar_kernel_size_.height;

  polar_kernel_center_point_ = cv::Point((polar_kernel_size_.width - 1) / 2,
                                         (polar_kernel_size_.height - 1) / 2);

  // Calculate probability values of Gaussian distribution according to target uncertainties
  gaussian_kernel_coefficients_ =
      cv::getGaussianKernel(polar_kernel_size_.height, target_range_sigma_pixels_) *
      cv::getGaussianKernel(polar_kernel_size_.width, target_azimuth_sigma_pixels_).t();

  // Set values outside of the +/- 3 sigma ellipse to remove artifacts caused by
  // rectangular kernels
  gaussian_kernel_coefficients_.setTo(
      0,
      cv::getStructuringElement(cv::MORPH_ELLIPSE, polar_kernel_size_) == 0);

  // Normalize all coefficients so the center point's value is 1, and then
  // convert to the corresponding ushort pixel intensities
  gaussian_kernel_coefficients_ /=
    gaussian_kernel_coefficients_.at<double>(polar_kernel_center_point_);
  gaussian_kernel_coefficients_.convertTo(gaussian_kernel_coefficients_,
                                          CV_16UC1,
                                          std::numeric_limits<ushort>::max());
}

void RadarImageGenerator::GeneratePolarImage(
    const std::vector<RadarDetection>& valid_radar_detections,
    cv::Mat& polar_image) {
  polar_image = cv::Mat::zeros(polar_image_height_, polar_image_width_, CV_16UC1);

  cv::Point target_point;
  cv::Rect target_rect, gaussian_rect;
  cv::Mat gaussian_kernel;

  for (const RadarDetection& radar_detection : valid_radar_detections) {
    // Do not add detection to emulated image if it was detected to be a dynamic agent
    if (!radar_detection.is_static()) continue;
    target_point = cv::Point(- radar_detection.theta / radians_per_pixel_ + polar_image_u0_,
                            - radar_detection.range / meters_per_pixel_ + polar_image_v0_);

    // Rectangle describing the pixels in the polar image that the individual Gaussian submatrix
    // will be added to
    target_rect = cv::Rect(target_point.x - polar_kernel_center_point_.x,
                           target_point.y - polar_kernel_center_point_.y,
                           polar_kernel_size_.width,
                           polar_kernel_size_.height);
    // Rectange describing the size of the individual Gaussian distribution submatrix
    gaussian_rect = cv::Rect(0, 0, polar_kernel_size_.width, polar_kernel_size_.height);

    // Trim submatrix rectangles to ensure they stay within image bounds. Only proceeds if at least
    // part of the rectangles are within the image
    if (!TrimRectangles(target_rect, gaussian_rect)) continue;

    // Make trimmed gaussian kernel whose pixel intensities are scaled according to its SNR-based
    // weight (center point corresponds to target detection's SNR)
    gaussian_kernel = gaussian_kernel_coefficients_(gaussian_rect).clone();
    gaussian_kernel *= radar_detection.weight;

    // Add gaussian distribution submatrix to its location within the RADAR polar image
    polar_image(target_rect) += gaussian_kernel;
  }
}

bool RadarImageGenerator::TrimRectangles(cv::Rect& target_rect, cv::Rect& gaussian_rect) {
    while (target_rect.x < 0) {
      ++target_rect.x;
      ++gaussian_rect.x;
      --target_rect.width;
      --gaussian_rect.width;
    }
    while (target_rect.y < 0) {
      ++target_rect.y;
      ++gaussian_rect.y;
      --target_rect.height;
      --gaussian_rect.height;
    }
    while ((target_rect.x + target_rect.width) >=
             static_cast<int>(polar_image_width_)) {
      --target_rect.width;
      --gaussian_rect.width;
    }
    while ((target_rect.y + target_rect.height) >=
             static_cast<int>(polar_image_height_)) {
      --target_rect.height;
      --gaussian_rect.height;
    }

    return (target_rect.width > 0 && target_rect.height > 0) ? true : false;
}

void RadarImageGenerator::GenerateSimulatedPolarImage(
      cv::Mat& polar_image) {
  // Creates a vector of "fake" simulated radar detections.
  // They are conveniently distributed in a grid formation.
  std::vector<RadarDetection> simulated_radar_detections;
  simulated_radar_detections.reserve(128);

  for (double range = meters_per_pixel_ * polar_image_height_ / 6.0;
       range < meters_per_pixel_ * polar_image_height_;
       range += meters_per_pixel_ * polar_image_height_ /  6.0) {
    for (double theta = - M_PI;
         theta < M_PI;
         theta += 1.0 / 4.0) {
      double new_theta = std::fmod(theta + simulated_theta_offset_, 2.0 * M_PI);
      new_theta = std::fmod((new_theta + 2.0 * M_PI), 2.0 * M_PI);
      if (new_theta > M_PI) new_theta -= 2.0 * M_PI;

      simulated_radar_detections.emplace_back(
        1.0,
        range,
        new_theta,
        0.0,
        1);
      simulated_radar_detections.back().MarkStatic();
    }
  }

  // Generates the polar image from the simualted radar detections
  GeneratePolarImage(simulated_radar_detections, polar_image);

  // Subtracts 0.1 rad from the theta offset so that the next RADAR image will correspond
  // to the sensor purely rotating +0.1 rad
  simulated_theta_offset_ -= 0.1;
}
