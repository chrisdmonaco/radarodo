/********************************COPYRIGHT CHRIS D. MONACO, 2019**********************************/
#pragma once

#include <vector>
#include <string>

#include <ros/ros.h>
#include <delphi_esr_msgs/EsrEthTx.h>
#include <opencv2/core/core.hpp>
#include <eigen3/Eigen/Core>
#include <std_msgs/Header.h>

#include <radarodo/radar_detection.h>
#include <radarodo/radar_image_generator.h>
#include <radarodo/trans_vel_estimator.h>
#include <radarodo/remapper.h>
#include <radarodo/rot_vel_estimator.h>
#include <radarodo/analyzer.h>

/**
 * @brief Defines the class for the "RADARODO" RADAR-based Ego-motion Estimator
          See "RADARODO: Ego-Motion Estimation from Doppler and Spatial Data in RADAR Images"
          (Monaco et al., 2019)
 * 
 */
class EgomotionEstimator {
 public:
  EgomotionEstimator();
 private:
  /**
   * @brief Callback whenever RADAR publishes data from its mid or long range scans
   * 
   * @param[in] radar_data_msg_ptr    Pointer for message from Delphi ESR 2.5 RADAR 
   *                                    as provided by AutonomouStuff ROS node
   *                                    ("Raw", unfiltered data via Ethernet)
   */
  void RadarDataCallback(const delphi_esr_msgs::EsrEthTxConstPtr radar_data_msg_ptr);

  /**
   * @brief Stores RADAR data and checks if both scans have been stored
   * 
   * @param[in] radar_data_msg    Radar data message from callback for mid or long range scan
   * 
   * @return true                 If both mid and long range scans have been stored for this cycle
   * @return false                Otherwise
   */
  bool StoreRadarData(const delphi_esr_msgs::EsrEthTx& radar_data_msg);

  /**
   * @brief Prepares for next measurement by extracting/storing RADAR image points and storing
   *        timestamp
   * 
   * @param[in] polar_image               Current unaltered RADAR polar image (mono16 datatype)
   * @param[in] previous_msg_timestamp    Current timestamp to be stored as the previous message 
   *                                      timestamp
   */
  void PrepareForNextMeasurement(const cv::Mat& polar_image,
                                 const ros::Time& previous_msg_timestamp);

  /**
   * @brief Formats sensor ego-motion estimate (and covariance) into timestamped message and 
   *        publishes over ROS
   * 
   * @param[in] radar_data_msg_header Header from current RADAR data ROS message
   * @param[in] sensor_motion_estimate Sensor motion estimate represented as the 3-element vector of
   *            its planar motion [v_x, v_y, omega_z]
   * @param[in] sensor_motion_covariance_matrix Sensor motion estimate expected covariance matrix
   *                                            represented as a coresponding 3 x 3 matrix
   */
  void PublishSensorMotionMsg(const std_msgs::Header& radar_data_msg_header,
                              const Eigen::Vector3d& sensor_motion_estimate,
                              const Eigen::Matrix<double, 3, 3>& sensor_motion_covariance_matrix);

  const std::string ros_node_path_;                     // string for file path that this ROS node is
                                                        //   running on
  ros::NodeHandle nh_;                                  // ROS nodehandle
  ros::Subscriber radar_data_subscriber_;               // ROS RADAR data subscriber
  ros::Publisher egomotion_publisher_;                  // ROS sensor ego-motion publisher
  bool mid_range_scan_stored_;                          // True if RADAR mid range scan data has 
                                                        //   been stored
  bool long_range_scan_stored_;                         // True if RADAR long range scan data has
                                                        //   been stored
  std::vector<RadarDetection> valid_radar_detections_;  // Vector that stores all valid data
                                                        //   detections for that cycle (both mid
                                                        //   and long range)
  cv::Mat polar_image_;                                 // RADAR polar image (mono16 datatype)
  RadarImageGenerator radar_image_generator_;           // RADAR Image Generator
  TransVelEstimator trans_vel_estimator_;               // Translational Velocity Estimator
  Remapper remapper_;                                   // Remapper (for images and points)
  RotVelEstimator rot_vel_estimator_;                   // Rotational Velocity Estimator
  Analyzer analyzer_;                                   // Error and computational performance
                                                        //   analyzer
  ros::Time previous_msg_timestamp_;                    // Previous message's timestamp
};
