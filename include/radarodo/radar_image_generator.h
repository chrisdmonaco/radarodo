/********************************COPYRIGHT CHRIS D. MONACO, 2019**********************************/
#pragma once

#include <vector>

#include <opencv2/core/core.hpp>

#include <radarodo/radar_detection.h>
/**
 * @brief Class to generate emulated RADAR images
 * 
 */
class RadarImageGenerator {
 public:
  /**
   * @brief Construct a new RadarImageGenerator object
   * 
   * @param[in] polar_image_width               Width of polar image, corr. to theta range [px.]
   * @param[in] polar_image_height              Height of polar image, corr. to max. range [px.]
   * @param[in] meters_per_pixel                Meters per pixel [m/px.]
   * @param[in] radians_per_pixel               Radians per pixel [rad/px.]
   * @param[in] polar_image_u0                  U-coordinate of polar image center
   * @param[in] polar_image_v0                  V-coordinate of polar image center
   * @param[in] target_range_sigma_meters       RADAR target range stadard deviation [m]
   * @param[in] target_azimuth_sigma_radians    RADAR target azimuthal angle stadard dev. [rad]
   */
  RadarImageGenerator(
    const uint polar_image_width,
    const uint polar_image_height,
    const double meters_per_pixel,
    const double radians_per_pixel,
    const double polar_image_u0,
    const double polar_image_v0,
    const double target_range_sigma_meters,
    const double target_azimuth_sigma_radians);

  /**
   * @brief Generates emulated polar image from valid detected RADAR targets
   * 
   * @param[in] valid_radar_detections    Vector of valid detected RADAR targets
   * @param[out] polar_image              CV_16UC1 RADAR polar image (Range-Azimuth heatmap)
   */
  void GeneratePolarImage(const std::vector<RadarDetection>& valid_radar_detections,
                          cv::Mat& polar_image);

  /**
   * @brief Generates a simulated polar image of a simulated environment that emulates a sensor 
   *        purely rotating 0.1 rad between measurements (FOR DEBUGGING PURPOSES ONLY)
   * 
   * @param polar_image[out]    CV_8UC1 RADAR polar image (Range-Azimuth heatmap)
   */
  void GenerateSimulatedPolarImage(cv::Mat& polar_image);

 private:
  /**
   * @brief Trims the rectangles associated with each target's individual Gaussian distribution
   *        contribution so they don't exceed image boundaries
   * 
   * @param[in, out] target_rect      Rectangle describing the pixels in the polar image that the
   *                                    individual Gaussian submatrix will be added to
   * @param[in, out] gaussian_rect    Rectangle describing the size of the individual Gaussian 
   *                                    distribution submatrix
   * @return true                     If any portion of the rectangle exists within image bounds
   * @return false                    Otherwise
   */
  bool TrimRectangles(cv::Rect& target_rect, cv::Rect& gaussian_rect);

  const uint polar_image_width_;                // Width of RADAR polar image [px.]
  const uint polar_image_height_;               // Height of RADAR polar image [px.]
  const double meters_per_pixel_;               // Meters per pixel [m/px.]
  const double radians_per_pixel_;              // Radians per pixel [rad/px.]
  const double polar_image_u0_;                 // U-coordinate of polar image center
  const double polar_image_v0_;                 // V-coordinate of polar image center
  const double target_range_sigma_pixels_;      // RADAR target range stadard deviation [m]
  const double target_azimuth_sigma_pixels_;    // RADAR target azimuthal angle stadard dev. [rad]
  cv::Size polar_kernel_size_;                  // Size of polar image kernel [px., px.] corr. to
                                                //    +/- 3 sigma of target position uncertainty
  cv::Point polar_kernel_center_point_;         // Center pixel location of polar kernel [px., px.]
  cv::Mat gaussian_kernel_coefficients_;        // Unscaled gaussian kernel coefficients (prob.
                                                //   values) determined by target pos.
                                                //   uncertainties
  double simulated_theta_offset_ = 0;           // Theta offset for simulated targets in simulated
                                                //   RADAR image (starts at 0 rad.)
};
