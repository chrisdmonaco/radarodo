/********************************COPYRIGHT CHRIS D. MONACO, 2019**********************************/
#pragma once

#include <g2o/core/base_unary_edge.h>
#include <eigen3/Eigen/Core>

#include <radarodo/heading_change_vertex.h>

namespace Radarodo {

/**
 * @brief Class that describes the prior edge constraining a HeadingChangeVertex pose vertex's
 *        heading change
 * 
 */
class HeadingChangeEdgePrior : public g2o::BaseUnaryEdge<1, double, HeadingChangeVertex> {
 public:
  void computeError() final {
    _error[0] = static_cast<const HeadingChangeVertex*>(_vertices[0])->estimate() - _measurement;
  }

  void linearizeOplus() final { _jacobianOplusXi[0] = -1.0; }

  bool read(std::istream& is) override { return is.good(); }
  bool write(std::ostream& os) const override { return os.good(); }
};

}  // namespace Radarodo
