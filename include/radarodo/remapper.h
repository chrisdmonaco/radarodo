/********************************COPYRIGHT CHRIS D. MONACO, 2019**********************************/
#pragma once

#include <opencv2/core/core.hpp>
#include <eigen3/Eigen/Core>

/**
 * @brief Class for remapping (or transforming) RADAR images or extracted
 *        representative pixel points
 */
class Remapper {
 public:
  /**
   * @brief Construct a new Remapper object
   * 
   * @param[in] long_range_max                    Long-range RADAR scan max range [m]
   * @param[in] long_range_plus_minus_fov_rad     +/- field of view for long-range RADAR scan [rad]
   * @param[in] mid_range_max                     Mid-range RADAR scan max range [m]
   * @param[in] mid_range_plus_minus_fov_rad      +/- field of view for long-range RADAR scan [rad]
   * @param[in] polar_image_width                 Width of polar RADAR image [px.]
   * @param[in] cartesian_image_width             Cartesian RADAR image width [px.]
   * @param[in] image_height                      RADAR image height [px.] (equal for Cartesian and
   *                                                polar images)
   * @param[in] meters_per_pixel                  Meters per pixel of RADAR images [m/px.]
   * @param[in] radians_per_pixel                 Radians per pixel of RADAR images [rad/px.]
   * @param[in] polar_image_u0                    U-coordinate of RADAR polar image center [px.]
   * @param[in] cartesian_image_u0                U-coordinate of RADAR cartesian image center [px.]
   * @param[in] image_v0                          V-coordinate of RADAR image center [px.] (equal 
   *                                                for Cartesian and polar images)
   */
  Remapper(const double long_range_max,
           const double long_range_plus_minus_fov_rad,
           const double mid_range_max,
           const double mid_range_plus_minus_fov_rad,
           const uint polar_image_width,
           const uint cartesian_image_width,
           const uint image_height,
           const double meters_per_pixel,
           const double radians_per_pixel,
           const double polar_image_u0,
           const double cartesian_image_u0,
           const double image_v0);

  /**
   * @brief Transforms a RADAR image from one coordinate system to another
   * 
   * @tparam From        Coordinate system the RADAR image is transformed from
   * @tparam To          Coordinate system the RADAR image is transformed to
   * @param[in] from     Original CV_16UC1 RADAR image 
   * @param[out] to      Transformed CV_16UC1 RADAR image
   */
  template<typename From, typename To>
  void Remap(const cv::Mat& from, cv::Mat& to);

  /**
   * @brief Blurs RADAR polar image according to translational velocity estimate
   *        uncertainty
   * 
   * @param[in] sensor_motion_covariance_matrix     3x3 covariance matrix of sensor motion estimate
   *                                                  uncertainty (corr. to v_x, v_y, omega_z) 
   * @param[in] delta_t                             time between RADAR image measurements [sec]
   * @param[in] polar_image                         CV_16UC1 RADAR polar image
   * @param[out] blurred_polar_image                CV_16UC1 blurred RADAR polar image
   */
  void GenerateBlurredPolarImage(
      const Eigen::Matrix<double, 3, 3>& sensor_motion_covariance_matrix,
      const double delta_t,
      cv::Mat& polar_image,
      cv::Mat& blurred_polar_image);

  /**
   * @brief Calculates how much the previously extracted pixel points should be translated
   *        in the current image to account for the currently estimated translational motion
   * 
   * @tparam Method                             Method for calculating translation
   *                                              (Exact or Linear Approximation)
   * @param[in] sensor_motion_estimate          3x1 vector of sensor motion estimate 
   *                                              (v_x, v_y, omega_z) [m/s, m/s, rad/s]
   * @param[in] delta_t                         Time between RADAR images [sec.]
   * @param[in] previous_polar_pixel_points     Nx2 matrix of the previously extracted pixel points
   *                                              Cols: u-coords., v-coords. [px.]
   * @param[out] polar_pixel_translations       Nx2 matrix of corresponding polar pixel 
   *                                              translations. Cols: u-coords., v-coords. [px.]
   */
  template<typename Method>
  void CalculatePolarPixelTranslations(
    const Eigen::Vector3d& sensor_motion_estimate,
    const double delta_t,
    const Eigen::Array<double, Eigen::Dynamic, 2>& previous_polar_pixel_points,
    Eigen::Array<double, Eigen::Dynamic, 2>& polar_pixel_translations);

  /**
   * @brief Returns the mask flagging which pixels in the RADAR polar image
   *        are outside the sensor's field of view
   * 
   * @return const cv::Mat&     CV_8UC1 Polar non-field-of-view-mask
   *                              (255 = non-field-of-view)
   */
  const cv::Mat& polar_non_field_of_view_mask();

  /**
  * @brief Returns the mask flagging which pixels in the RADAR cartesian image
  *        are outside the sensor's field of view
  * 
  * @return const cv::Mat&     CV_8UC1 Cartesian non-field-of-view-mask
  *                            (255 = non-field-of-view)
  */
  const cv::Mat& cartesian_non_field_of_view_mask();

  struct Polar;       // Polar coordinate system
  struct Cartesian;   // Cartesian coordinate system
  struct Exact;       // Exact analytical relationships
  struct Approx;      // Linear approximations of analytical relationships
 private:
  /**
   * @brief Generates the pixel map necessary to transform images between coordinate systems
   * 
   * @tparam From     Coordinate system the RADAR image is transformed from
   * @tparam To       Coordinate system the RADAR image is transformed to
   */
  template<typename From, typename To>
  void GenerateMap();

  /**
   * @brief Generates the non-field-of-view-mask for the RADAR image
   * 
   * @tparam CoordinateSystem     Coordinate system of the image and mask
   */
  template<typename CoordinateSystem>
  void GenerateMask();

  /**
   * @brief Generates images with pixels that represent their u- or v- pixel coordinates
   * 
   * @param[in] image_width        Width of image for pixel coordinate labels[uint]
   * @param[in] image_height       Height of image for pixel coordinate labels [uint]
   * @param[out] u_values_image    CV_32FC1 image with pixels containing their u- pixel coordinates
   * @param[out] v_values_image    CV_32FC1 image with pixels containing their v- pixel coordinates
   */
  void GeneratePixelCoordinateImages(const uint image_width,
                                     const uint image_height,
                                     cv::Mat& u_values_image,
                                     cv::Mat& v_values_image);

  const double long_range_max_;                 // Long-range RADAR scan max range [m]
  const double long_range_plus_minus_fov_rad_;  // +/- field of view for long-range RADAR scan [rad]
  const double mid_range_max_;                  // Mid-range RADAR scan max range [m]
  const double mid_range_plus_minus_fov_rad_;   // +/- field of view for long-range RADAR scan [rad]
  const uint polar_image_width_;                // Width of polar RADAR image [px.]
  const uint cartesian_image_width_;            // Cartesian RADAR image width [px.]
  const uint image_height_;                     // RADAR image height [px.] (equal for Cartesian and
                                                //   polar images)
  const double meters_per_pixel_;               // Meters per pixel of RADAR images [m/px.]
  const double radians_per_pixel_;              // Radians per pixel of RADAR images [rad/px.]
  const double polar_image_u0_;                 // U-coordinate of RADAR polar image center [px.]
  const double cartesian_image_u0_;             // U-coordinate of cartesian image center [px.]
  const double image_v0_;                       // V-coordinate of RADAR image center [px.] (equal
                                                //   for Cartesian and polar images)
  cv::Mat polar2cart_u_map_;                    // Polar to Cart. transform u-coordinate pixel map
  cv::Mat polar2cart_v_map_;                    // Polar to Cart. transform v-coordinate pixel map
  cv::Mat cart2polar_u_map_;                    // Cart. to Polar transform u-coordinate pixel map
  cv::Mat cart2polar_v_map_;                    // Cart. to Polar transform v-coordinate pixel map
  cv::Mat polar_non_field_of_view_mask_;        // Polar RADAR image non-field-of-view mask
  cv::Mat cartesian_non_field_of_view_mask_;    // Cartesian RADAR image non-field-of-view mask
};

////////////// TEMPLATE SPECIALIZATIONS: //////////////
template <>
void Remapper::Remap<Remapper::Polar, Remapper::Cartesian>(const cv::Mat& from, cv::Mat& to);

template <>
void Remapper::Remap<Remapper::Cartesian, Remapper::Polar>(const cv::Mat& from, cv::Mat& to);

template <>
void Remapper::CalculatePolarPixelTranslations<Remapper::Exact>(
    const Eigen::Vector3d& sensor_motion_estimate,
    const double delta_t,
    const Eigen::Array<double, Eigen::Dynamic, 2>& previous_polar_pixel_points,
    Eigen::Array<double, Eigen::Dynamic, 2>& polar_pixel_translations);

template <>
void Remapper::CalculatePolarPixelTranslations<Remapper::Approx>(
    const Eigen::Vector3d& sensor_motion_estimate,
    const double delta_t,
    const Eigen::Array<double, Eigen::Dynamic, 2>& previous_polar_pixel_points,
    Eigen::Array<double, Eigen::Dynamic, 2>& polar_pixel_translations);

template <>
void Remapper::GenerateMap<Remapper::Polar, Remapper::Cartesian>();

template <>
void Remapper::GenerateMap<Remapper::Cartesian, Remapper::Polar>();

template<>
void Remapper::GenerateMask<Remapper::Polar>();

template<>
void Remapper::GenerateMask<Remapper::Cartesian>();
